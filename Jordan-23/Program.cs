﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jordan_23
{
    public class DataHandler
    {
        private DataHandler(DataHandler copy)
        {
            this.Data = copy.Data.Clone() as double[,];
        }
        public DataHandler(int n, int k)
        {
            Data = new double[n, k];
        }

        public double this[int x, int y]
        {
            get
            {
                return Data[x, y];
            }
            set
            {
                Data[x, y] = value;
            }
        }

        public DataHandler Clone()
        {
            return new DataHandler(this);
        }

        private double[,] Data
        {
            get;
            set;
        }
        private IEnumerable<T> Flatten<T>(T[,] map)
        {
            for (int row = 0; row < map.GetLength(0); row++)
            {
                for (int col = 0; col < map.GetLength(1); col++)
                {
                    yield return map[row, col];
                }
            }
        }

        override public string ToString() 
        {
            string forWriting = String.Empty;
            int i = 0;
            foreach (var arrElement in Flatten<double>(Data))
            {
                if (i != 0 && i % (Data.GetLength(1) - 1) == 0)
                {
                    forWriting += arrElement.ToString() + " " + "\r\n";
                    i = 0;
                    continue;
                }
                else
                {
                    forWriting += arrElement.ToString() + " ";
                    i++;
                }
            }
            return forWriting;
        }

    }
    public class Combinations
    {
        private List<int> _array; 
        private int _k; 
        private int[] _pushForward; 
        private List<int[]> _results; 
        private int[] _element; 

        public List<int[]> GetAllCombinations(List<int> _array2, int k)
        {
            int len = _array2.Count;
            if (len < k)
                throw new ArgumentException("Array length can't be less than number of selected elements");

            if (k < 1)
                throw new ArgumentException("Number of selected elements can't be less than 1");

            _array = _array2;
            _k = k;
            _results = new List<int[]>();
            _element = new int[k];
            _pushForward = new int[k]; 
            int maxStepsForward = len - _k + 1;

            GetCombinations(0, maxStepsForward);

            return _results;
        }

        private void GetCombinations(int col, int maxSteps)
        {
            for (int j = col + _pushForward[col]; j < maxSteps; j++)
            {
                _element[col] = _array[j];

                if (col < _k - 1)
                {
                    GetCombinations(col + 1, maxSteps + 1);
                }
                else if (col == _k - 1)
                {
                    int[] insert = new int[_k];
                    _element.CopyTo(insert, 0);
                    _results.Add(insert);
                }
            }
            if (col > 0)
            {
                _pushForward[col - 1]++;
                for (int k = col; k < _k; k++)
                    _pushForward[k] = _pushForward[col - 1];
            }
        }
    }

    public class Program
    {
        #region Variables
        public List<int> ForCombinations
        {
            get
            {
                return new List<int>(Enumerable.Range(1, K - 1));
            }
        }

        public DataHandler InitialArray
        {
            get;
            set;
        }

        public int K
        {
            get;
            set;
        }
        public int N
        {
            get;
            set;
        }

        #endregion

        
        public void ReadMatrix(string input)
        {
            int i = 0, j = 0;
            N = input.Split('\n').Length;
            K = (input.Split('\n').GetValue(0) as string).Split(' ').Length;
            InitialArray = new DataHandler(N, K);
            foreach (var row in input.Split('\n'))
            {
                j = 0;
                foreach (var col in row.Trim().Split(' '))
                {
                    InitialArray[i, j] = int.Parse(col.Trim());
                    j++;
                }
                i++;
            }
        }

        public List<int[]> Combinations
        {
            get
            {
                return (new Combinations()).GetAllCombinations(ForCombinations, N);
            }
        }

        public IEnumerable<DataHandler> CalcJordanExc(int[] arr)
        {
            List<DataHandler> results = new List<DataHandler>();
            DataHandler previous = InitialArray;
            DataHandler initial = InitialArray.Clone();
            DataHandler array;

            for (int i = 0; i < N; i++)
            {
                array = new DataHandler(N, K);
                for (int j = 0; j < N; j++)
                {
                    for (int k = 0; k < K; k++)
                    {
                        if (j == i)
                        {
                            array[j, k] = previous[j, k] / previous[i, arr[i] - 1];
                        }
                        else if (k == arr[i] - 1)
                        {
                            array[j, k] = 0;
                        }
                        else
                        {
                            array[j, k] = ((previous[j, k] * previous[i, arr[i] - 1])
                                - (previous[j, arr[i] - 1] * previous[i, k]))
                            / previous[i, arr[i] - 1];
                        }
                    }
                }
                previous = array;
                yield return array;
            }
            InitialArray = initial;
        }

        [STAThread]
        static void Main(string[] args)
        {
            Program program = new Program();

            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    try
                    {
                        program.ReadMatrix(File.ReadAllText(fbd.FileName));
                    }
                    catch(Exception e)
                    {
                        Console.Write(e.Message);
                    }
                }
            }

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    try
                    {
                        foreach (var combination in program.Combinations)
                        {
                            string currentPath =  fbd.SelectedPath + "\\" + string.Concat(combination) + ".txt";

                            using (StreamWriter file = new StreamWriter(currentPath))
                            {
                                foreach (var element in program.CalcJordanExc(combination))
                                {
                                    file.Write(element.ToString() + file.NewLine);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Write(e.Message);
                    }
                }
            }
        }
    }
}